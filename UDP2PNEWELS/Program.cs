﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Diagnostics;
using System.Threading;

namespace UDP2PNEWELS
{

    public class Program
    {
        public static class Globals
        {
            public static int log_1 = 0; // Modifiable in Code
            //public const Int32 VALUE = 10; // Unmodifiable
        }

        public static void UdpProcess(string IP, int port, string welcome)
        {
            try
            {
                TimeSpan t = DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Unspecified));
                int epoch = (int)t.TotalSeconds + 28800;
                DateTime epoch1 = new DateTime(1970, 1, 1);
                DateTime myDateTime = epoch1.AddSeconds(epoch);
                var form1 = new Form1();
                byte[] data = new byte[1024];
                string input, stringData;
                UdpClient server = new UdpClient(IP, port);
                IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
                Console.WriteLine("Starting service...");
                Console.WriteLine("Service started.");
                //welcome = "+";
                data = Encoding.ASCII.GetBytes(welcome);
                server.Send(data, data.Length);
                data = server.Receive(ref sender);
                Console.WriteLine("Message received from {0}:", sender.ToString());
                stringData = Encoding.ASCII.GetString(data, 0, data.Length);
                Console.WriteLine("(" + myDateTime + ")  " + stringData);
                myform.received_data(stringData);
                while (true)
                {
                    input = "+";//Console.ReadLine();
                    if (input == "exit")
                        break;

                    server.Send(Encoding.ASCII.GetBytes(input), input.Length);
                    data = server.Receive(ref sender);
                    stringData = Encoding.ASCII.GetString(data, 0, data.Length);
                    t = DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Unspecified));
                    epoch = (int)t.TotalSeconds + 28800;
                    myDateTime = epoch1.AddSeconds(epoch);
                    Console.WriteLine("(" + myDateTime + ")  " + stringData);
                    myform.received_data(stringData);

                }
                Console.WriteLine("Stopping service...");
                server.Close();
                Console.WriteLine("Service stopped.");
                Thread.CurrentThread.Abort();
                //Application.Exit();
            }
            catch(Exception ex)
            {
                if(Program.Globals.log_1 == 0)
                    MessageBox.Show("Oh no. We are crashed! :(. Call the ambulance!!\r\n" + ex);
                Form1 master = (Form1)Application.OpenForms["Form1"];
                master.buttonDisconnect.PerformClick();
            }
        }

        static byte[] HexToBytes(string input)
        {
            byte[] result = new byte[input.Length];
            char[] chars = input.ToCharArray();
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = Convert.ToByte(chars[i]);
            }
            return result;
        }

        public static class crc16
        {
            const ushort polynomial = 0xA001;
            static readonly ushort[] table = new ushort[256];

            public static ushort ComputeChecksum(byte[] bytes)
            {
                ushort crc = 0;
                for (int i = 0; i < bytes.Length; i++)
                {
                    byte index = (byte)(crc ^ bytes[i]);
                    crc = (ushort)((crc >> 8) ^ table[index]);
                }
                return crc;
            }

            static crc16()
            {
                ushort value;
                ushort temp;
                for (ushort i = 0; i < table.Length; i++)
                {
                    value = 0;
                    temp = i;
                    for (byte j = 0; j < 8; j++)
                    {
                        if (((value ^ temp) & 0x0001) != 0)
                        {
                            value = (ushort)((value >> 1) ^ polynomial);
                        }
                        else
                        {
                            value >>= 1;
                        }
                        temp >>= 1;
                    }
                    table[i] = value;
                }
            }
        }

        public static string GenerateCommand(int epoch)
        {
            try
            {
                string command = "[timech]";
                string time = Convert.ToString(epoch);
                string fullCommand = command + "[" + time + "]";
                var bytes = HexToBytes(fullCommand);
                string hex = crc16.ComputeChecksum(bytes).ToString("x2");
                fullCommand = fullCommand + hex;
                return fullCommand;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception: " + ex.Message);
                return "Error!";
            }
        }

        public static string ByteToString(int epoch, string command)
        {
            string time = null;
            string fullCommand = null;
            if(epoch == 0)
            {
                fullCommand = command;
            }
            else
            {
                time = Convert.ToString(epoch);
                fullCommand = command + "[" + time + "]";
            }
            //string time = Convert.ToString(epoch);
            //string fullCommand = command + "[" + time + "]";
            var bytes = HexToBytes(fullCommand);
            string crc = crc16.ComputeChecksum(bytes).ToString("x4");
            UInt16 rsize = 0;
            byte[] buff = new byte[16];
            string hex_string = "0123456789abcdef";
            string result;
            char[] hex = hex_string.ToCharArray();

            for (int i = 0; i < hex.Length; i++)
            {
                buff[i] = Convert.ToByte(hex[i]);
            }

            byte[] output = new byte[bytes.Length * 2];

            for (int i = 0; i < bytes.Length; i++)
            {
                output[rsize++] = buff[(bytes[i] >> 4) & 0xf];
                output[rsize++] = buff[bytes[i] & 0xf];
            }

            result = Encoding.ASCII.GetString(output);
            return result + crc;

        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 
        static Form1 myform;

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            myform = new Form1();
            Application.Run(myform);
        }
    }
}
