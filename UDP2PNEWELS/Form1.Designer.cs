﻿namespace UDP2PNEWELS
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxIP = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxPort = new System.Windows.Forms.TextBox();
            this.buttonConnect = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxDeviceAddr = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonSend = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.buttonDisconnect = new System.Windows.Forms.Button();
            this.textBoxDebug = new System.Windows.Forms.TextBox();
            this.textBoxAC = new System.Windows.Forms.TextBox();
            this.textBoxSoftManual = new System.Windows.Forms.TextBox();
            this.textBoxBattStat = new System.Windows.Forms.TextBox();
            this.textBoxTemp = new System.Windows.Forms.TextBox();
            this.textBoxVcharge = new System.Windows.Forms.TextBox();
            this.textBoxFactory = new System.Windows.Forms.TextBox();
            this.textBoxManual = new System.Windows.Forms.TextBox();
            this.textBoxWPS = new System.Windows.Forms.TextBox();
            this.textBoxCharge = new System.Windows.Forms.TextBox();
            this.textBoxDisc = new System.Windows.Forms.TextBox();
            this.textBoxStatus = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.textBoxTempValue = new System.Windows.Forms.TextBox();
            this.textBoxBattValue = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.textBoxTime = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label27 = new System.Windows.Forms.Label();
            this.textBoxRSSI = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.textBoxMessage = new System.Windows.Forms.TextBox();
            this.textBoxCommand = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.textBox2ndlog = new System.Windows.Forms.TextBox();
            this.textBox1stlog = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.radioButtonEmergy = new System.Windows.Forms.RadioButton();
            this.radioButtonDischarge = new System.Windows.Forms.RadioButton();
            this.radioButtonTime = new System.Windows.Forms.RadioButton();
            this.radioButtonCharge = new System.Windows.Forms.RadioButton();
            this.labelConnected = new System.Windows.Forms.Label();
            this.labelDiscon = new System.Windows.Forms.Label();
            this.labelResult = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Enter Gateway IP and Port:";
            // 
            // textBoxIP
            // 
            this.textBoxIP.Location = new System.Drawing.Point(83, 32);
            this.textBoxIP.Name = "textBoxIP";
            this.textBoxIP.Size = new System.Drawing.Size(97, 20);
            this.textBoxIP.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "IP Address:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(186, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Port:";
            // 
            // textBoxPort
            // 
            this.textBoxPort.Location = new System.Drawing.Point(222, 32);
            this.textBoxPort.Name = "textBoxPort";
            this.textBoxPort.Size = new System.Drawing.Size(100, 20);
            this.textBoxPort.TabIndex = 4;
            // 
            // buttonConnect
            // 
            this.buttonConnect.Location = new System.Drawing.Point(392, 30);
            this.buttonConnect.Name = "buttonConnect";
            this.buttonConnect.Size = new System.Drawing.Size(75, 23);
            this.buttonConnect.TabIndex = 5;
            this.buttonConnect.Text = "Connect";
            this.buttonConnect.UseVisualStyleBackColor = true;
            this.buttonConnect.Click += new System.EventHandler(this.buttonConnect_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 214);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Device address:";
            this.label4.Visible = false;
            // 
            // textBoxDeviceAddr
            // 
            this.textBoxDeviceAddr.Location = new System.Drawing.Point(101, 211);
            this.textBoxDeviceAddr.Name = "textBoxDeviceAddr";
            this.textBoxDeviceAddr.Size = new System.Drawing.Size(82, 20);
            this.textBoxDeviceAddr.TabIndex = 8;
            this.textBoxDeviceAddr.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 245);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Selected command:";
            // 
            // buttonSend
            // 
            this.buttonSend.Location = new System.Drawing.Point(367, 240);
            this.buttonSend.Name = "buttonSend";
            this.buttonSend.Size = new System.Drawing.Size(75, 23);
            this.buttonSend.TabIndex = 11;
            this.buttonSend.Text = "Send";
            this.buttonSend.UseVisualStyleBackColor = true;
            this.buttonSend.Click += new System.EventHandler(this.buttonSend_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(200, 214);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(138, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "EEPROM address (0-8192):";
            this.label6.Visible = false;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(344, 211);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(98, 20);
            this.textBox1.TabIndex = 13;
            this.textBox1.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Lucida Sans Unicode", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(475, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 20);
            this.label7.TabIndex = 14;
            this.label7.Text = "Status:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(479, 44);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "AC Input:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(479, 128);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(101, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Soft Manual Button:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(479, 86);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "Battery Status:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(479, 107);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(103, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "Temperature Status:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(479, 65);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(124, 13);
            this.label12.TabIndex = 19;
            this.label12.Text = "Charging Voltage Status:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(479, 149);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(79, 13);
            this.label13.TabIndex = 20;
            this.label13.Text = "Factory Button:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(479, 170);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(79, 13);
            this.label14.TabIndex = 21;
            this.label14.Text = "Manual Button:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(479, 191);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(69, 13);
            this.label15.TabIndex = 22;
            this.label15.Text = "WPS Button:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(479, 212);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(74, 13);
            this.label16.TabIndex = 23;
            this.label16.Text = "Current Mode:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(479, 233);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(114, 13);
            this.label17.TabIndex = 24;
            this.label17.Text = "Soft Discharge Button:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(479, 254);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(100, 13);
            this.label18.TabIndex = 25;
            this.label18.Text = "Soft Charge Button:";
            // 
            // buttonDisconnect
            // 
            this.buttonDisconnect.Location = new System.Drawing.Point(392, 29);
            this.buttonDisconnect.Name = "buttonDisconnect";
            this.buttonDisconnect.Size = new System.Drawing.Size(75, 23);
            this.buttonDisconnect.TabIndex = 26;
            this.buttonDisconnect.Text = "Disconnect";
            this.buttonDisconnect.UseVisualStyleBackColor = true;
            this.buttonDisconnect.Visible = false;
            this.buttonDisconnect.Click += new System.EventHandler(this.buttonDisconnect_Click);
            // 
            // textBoxDebug
            // 
            this.textBoxDebug.Location = new System.Drawing.Point(12, 336);
            this.textBoxDebug.Name = "textBoxDebug";
            this.textBoxDebug.ReadOnly = true;
            this.textBoxDebug.Size = new System.Drawing.Size(687, 20);
            this.textBoxDebug.TabIndex = 27;
            this.textBoxDebug.TextChanged += new System.EventHandler(this.textBoxDebug_TextChanged);
            // 
            // textBoxAC
            // 
            this.textBoxAC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxAC.Location = new System.Drawing.Point(606, 37);
            this.textBoxAC.Name = "textBoxAC";
            this.textBoxAC.ReadOnly = true;
            this.textBoxAC.Size = new System.Drawing.Size(93, 20);
            this.textBoxAC.TabIndex = 28;
            // 
            // textBoxSoftManual
            // 
            this.textBoxSoftManual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxSoftManual.Location = new System.Drawing.Point(606, 121);
            this.textBoxSoftManual.Name = "textBoxSoftManual";
            this.textBoxSoftManual.ReadOnly = true;
            this.textBoxSoftManual.Size = new System.Drawing.Size(93, 20);
            this.textBoxSoftManual.TabIndex = 28;
            // 
            // textBoxBattStat
            // 
            this.textBoxBattStat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxBattStat.Location = new System.Drawing.Point(606, 79);
            this.textBoxBattStat.Name = "textBoxBattStat";
            this.textBoxBattStat.ReadOnly = true;
            this.textBoxBattStat.Size = new System.Drawing.Size(93, 20);
            this.textBoxBattStat.TabIndex = 28;
            // 
            // textBoxTemp
            // 
            this.textBoxTemp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxTemp.Location = new System.Drawing.Point(606, 100);
            this.textBoxTemp.Name = "textBoxTemp";
            this.textBoxTemp.ReadOnly = true;
            this.textBoxTemp.Size = new System.Drawing.Size(93, 20);
            this.textBoxTemp.TabIndex = 28;
            // 
            // textBoxVcharge
            // 
            this.textBoxVcharge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxVcharge.Location = new System.Drawing.Point(606, 58);
            this.textBoxVcharge.Name = "textBoxVcharge";
            this.textBoxVcharge.ReadOnly = true;
            this.textBoxVcharge.Size = new System.Drawing.Size(93, 20);
            this.textBoxVcharge.TabIndex = 28;
            // 
            // textBoxFactory
            // 
            this.textBoxFactory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxFactory.Location = new System.Drawing.Point(606, 142);
            this.textBoxFactory.Name = "textBoxFactory";
            this.textBoxFactory.ReadOnly = true;
            this.textBoxFactory.Size = new System.Drawing.Size(93, 20);
            this.textBoxFactory.TabIndex = 28;
            // 
            // textBoxManual
            // 
            this.textBoxManual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxManual.Location = new System.Drawing.Point(606, 163);
            this.textBoxManual.Name = "textBoxManual";
            this.textBoxManual.ReadOnly = true;
            this.textBoxManual.Size = new System.Drawing.Size(93, 20);
            this.textBoxManual.TabIndex = 28;
            // 
            // textBoxWPS
            // 
            this.textBoxWPS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxWPS.Location = new System.Drawing.Point(606, 184);
            this.textBoxWPS.Name = "textBoxWPS";
            this.textBoxWPS.ReadOnly = true;
            this.textBoxWPS.Size = new System.Drawing.Size(93, 20);
            this.textBoxWPS.TabIndex = 28;
            // 
            // textBoxCharge
            // 
            this.textBoxCharge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxCharge.Location = new System.Drawing.Point(606, 247);
            this.textBoxCharge.Name = "textBoxCharge";
            this.textBoxCharge.ReadOnly = true;
            this.textBoxCharge.Size = new System.Drawing.Size(93, 20);
            this.textBoxCharge.TabIndex = 29;
            // 
            // textBoxDisc
            // 
            this.textBoxDisc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxDisc.Location = new System.Drawing.Point(606, 226);
            this.textBoxDisc.Name = "textBoxDisc";
            this.textBoxDisc.ReadOnly = true;
            this.textBoxDisc.Size = new System.Drawing.Size(93, 20);
            this.textBoxDisc.TabIndex = 30;
            // 
            // textBoxStatus
            // 
            this.textBoxStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxStatus.Location = new System.Drawing.Point(606, 205);
            this.textBoxStatus.Name = "textBoxStatus";
            this.textBoxStatus.ReadOnly = true;
            this.textBoxStatus.Size = new System.Drawing.Size(93, 20);
            this.textBoxStatus.TabIndex = 31;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(479, 275);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(70, 13);
            this.label19.TabIndex = 32;
            this.label19.Text = "Temperature:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(479, 296);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(82, 13);
            this.label20.TabIndex = 33;
            this.label20.Text = "Battery Voltage:";
            // 
            // textBoxTempValue
            // 
            this.textBoxTempValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxTempValue.Location = new System.Drawing.Point(606, 268);
            this.textBoxTempValue.Name = "textBoxTempValue";
            this.textBoxTempValue.ReadOnly = true;
            this.textBoxTempValue.Size = new System.Drawing.Size(93, 20);
            this.textBoxTempValue.TabIndex = 34;
            // 
            // textBoxBattValue
            // 
            this.textBoxBattValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxBattValue.Location = new System.Drawing.Point(606, 289);
            this.textBoxBattValue.Name = "textBoxBattValue";
            this.textBoxBattValue.ReadOnly = true;
            this.textBoxBattValue.Size = new System.Drawing.Size(93, 20);
            this.textBoxBattValue.TabIndex = 35;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(479, 317);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(89, 13);
            this.label21.TabIndex = 36;
            this.label21.Text = "Device RealTime";
            // 
            // textBoxTime
            // 
            this.textBoxTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxTime.Location = new System.Drawing.Point(574, 310);
            this.textBoxTime.Name = "textBoxTime";
            this.textBoxTime.ReadOnly = true;
            this.textBoxTime.Size = new System.Drawing.Size(125, 20);
            this.textBoxTime.TabIndex = 37;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.textBoxRSSI);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Controls.Add(this.textBoxMessage);
            this.groupBox1.Controls.Add(this.textBoxCommand);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.textBox2ndlog);
            this.groupBox1.Controls.Add(this.textBox1stlog);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.radioButtonEmergy);
            this.groupBox1.Controls.Add(this.radioButtonDischarge);
            this.groupBox1.Controls.Add(this.radioButtonTime);
            this.groupBox1.Controls.Add(this.radioButtonCharge);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textBoxDeviceAddr);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.buttonSend);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Location = new System.Drawing.Point(19, 65);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(448, 265);
            this.groupBox1.TabIndex = 42;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Command";
            this.groupBox1.Visible = false;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(364, 28);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(28, 13);
            this.label27.TabIndex = 52;
            this.label27.Text = "dBm";
            // 
            // textBoxRSSI
            // 
            this.textBoxRSSI.Location = new System.Drawing.Point(325, 25);
            this.textBoxRSSI.Name = "textBoxRSSI";
            this.textBoxRSSI.ReadOnly = true;
            this.textBoxRSSI.Size = new System.Drawing.Size(33, 20);
            this.textBoxRSSI.TabIndex = 51;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(281, 28);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(38, 13);
            this.label26.TabIndex = 50;
            this.label26.Text = "RSSI: ";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(7, 28);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(127, 13);
            this.label25.TabIndex = 49;
            this.label25.Text = "Select Device to Monitor:";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.comboBox1.Location = new System.Drawing.Point(137, 25);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 48;
            this.comboBox1.TextChanged += new System.EventHandler(this.comboBox1_TextChanged);
            // 
            // textBoxMessage
            // 
            this.textBoxMessage.Location = new System.Drawing.Point(113, 165);
            this.textBoxMessage.Name = "textBoxMessage";
            this.textBoxMessage.ReadOnly = true;
            this.textBoxMessage.Size = new System.Drawing.Size(329, 20);
            this.textBoxMessage.TabIndex = 47;
            // 
            // textBoxCommand
            // 
            this.textBoxCommand.Location = new System.Drawing.Point(110, 242);
            this.textBoxCommand.Name = "textBoxCommand";
            this.textBoxCommand.ReadOnly = true;
            this.textBoxCommand.Size = new System.Drawing.Size(248, 20);
            this.textBoxCommand.TabIndex = 43;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(9, 167);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(98, 13);
            this.label24.TabIndex = 46;
            this.label24.Text = "Gateway Message:";
            // 
            // textBox2ndlog
            // 
            this.textBox2ndlog.Location = new System.Drawing.Point(75, 137);
            this.textBox2ndlog.Name = "textBox2ndlog";
            this.textBox2ndlog.ReadOnly = true;
            this.textBox2ndlog.Size = new System.Drawing.Size(367, 20);
            this.textBox2ndlog.TabIndex = 45;
            // 
            // textBox1stlog
            // 
            this.textBox1stlog.Location = new System.Drawing.Point(75, 119);
            this.textBox1stlog.Name = "textBox1stlog";
            this.textBox1stlog.ReadOnly = true;
            this.textBox1stlog.Size = new System.Drawing.Size(367, 20);
            this.textBox1stlog.TabIndex = 44;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 140);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(49, 13);
            this.label23.TabIndex = 43;
            this.label23.Text = "2nd Log:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 121);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(45, 13);
            this.label22.TabIndex = 42;
            this.label22.Text = "1st Log:";
            // 
            // radioButtonEmergy
            // 
            this.radioButtonEmergy.AutoSize = true;
            this.radioButtonEmergy.Location = new System.Drawing.Point(6, 63);
            this.radioButtonEmergy.Name = "radioButtonEmergy";
            this.radioButtonEmergy.Size = new System.Drawing.Size(60, 17);
            this.radioButtonEmergy.TabIndex = 38;
            this.radioButtonEmergy.TabStop = true;
            this.radioButtonEmergy.Text = "Emergy";
            this.radioButtonEmergy.UseVisualStyleBackColor = true;
            this.radioButtonEmergy.Click += new System.EventHandler(this.radioButtonEmergy_Click);
            // 
            // radioButtonDischarge
            // 
            this.radioButtonDischarge.AutoSize = true;
            this.radioButtonDischarge.Location = new System.Drawing.Point(6, 86);
            this.radioButtonDischarge.Name = "radioButtonDischarge";
            this.radioButtonDischarge.Size = new System.Drawing.Size(105, 17);
            this.radioButtonDischarge.TabIndex = 41;
            this.radioButtonDischarge.TabStop = true;
            this.radioButtonDischarge.Text = "Discharging Test";
            this.radioButtonDischarge.UseVisualStyleBackColor = true;
            // 
            // radioButtonTime
            // 
            this.radioButtonTime.AutoSize = true;
            this.radioButtonTime.Location = new System.Drawing.Point(143, 63);
            this.radioButtonTime.Name = "radioButtonTime";
            this.radioButtonTime.Size = new System.Drawing.Size(86, 17);
            this.radioButtonTime.TabIndex = 39;
            this.radioButtonTime.TabStop = true;
            this.radioButtonTime.Text = "Update Time";
            this.radioButtonTime.UseVisualStyleBackColor = true;
            // 
            // radioButtonCharge
            // 
            this.radioButtonCharge.AutoSize = true;
            this.radioButtonCharge.Location = new System.Drawing.Point(143, 86);
            this.radioButtonCharge.Name = "radioButtonCharge";
            this.radioButtonCharge.Size = new System.Drawing.Size(91, 17);
            this.radioButtonCharge.TabIndex = 40;
            this.radioButtonCharge.TabStop = true;
            this.radioButtonCharge.Text = "Charging Test";
            this.radioButtonCharge.UseVisualStyleBackColor = true;
            // 
            // labelConnected
            // 
            this.labelConnected.AutoSize = true;
            this.labelConnected.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelConnected.ForeColor = System.Drawing.Color.SeaGreen;
            this.labelConnected.Location = new System.Drawing.Point(543, 8);
            this.labelConnected.Name = "labelConnected";
            this.labelConnected.Size = new System.Drawing.Size(117, 20);
            this.labelConnected.TabIndex = 44;
            this.labelConnected.Text = "CONNECTED";
            this.labelConnected.Visible = false;
            // 
            // labelDiscon
            // 
            this.labelDiscon.AutoSize = true;
            this.labelDiscon.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDiscon.ForeColor = System.Drawing.Color.Red;
            this.labelDiscon.Location = new System.Drawing.Point(543, 7);
            this.labelDiscon.Name = "labelDiscon";
            this.labelDiscon.Size = new System.Drawing.Size(148, 20);
            this.labelDiscon.TabIndex = 45;
            this.labelDiscon.Text = "DISCONNECTED";
            // 
            // labelResult
            // 
            this.labelResult.AutoSize = true;
            this.labelResult.Location = new System.Drawing.Point(350, 35);
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(35, 13);
            this.labelResult.TabIndex = 0;
            this.labelResult.Text = "label1";
            this.labelResult.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(711, 361);
            this.Controls.Add(this.labelResult);
            this.Controls.Add(this.labelDiscon);
            this.Controls.Add(this.labelConnected);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.textBoxTime);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.textBoxBattValue);
            this.Controls.Add(this.textBoxTempValue);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.textBoxCharge);
            this.Controls.Add(this.textBoxDisc);
            this.Controls.Add(this.textBoxStatus);
            this.Controls.Add(this.textBoxWPS);
            this.Controls.Add(this.textBoxManual);
            this.Controls.Add(this.textBoxFactory);
            this.Controls.Add(this.textBoxVcharge);
            this.Controls.Add(this.textBoxTemp);
            this.Controls.Add(this.textBoxBattStat);
            this.Controls.Add(this.textBoxSoftManual);
            this.Controls.Add(this.textBoxAC);
            this.Controls.Add(this.textBoxDebug);
            this.Controls.Add(this.buttonDisconnect);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.buttonConnect);
            this.Controls.Add(this.textBoxPort);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxIP);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "PNEWELS Communication";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxIP;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxPort;
        private System.Windows.Forms.Button buttonConnect;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxDeviceAddr;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button buttonSend;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBoxDebug;
        private System.Windows.Forms.TextBox textBoxAC;
        private System.Windows.Forms.TextBox textBoxSoftManual;
        private System.Windows.Forms.TextBox textBoxBattStat;
        private System.Windows.Forms.TextBox textBoxTemp;
        private System.Windows.Forms.TextBox textBoxVcharge;
        private System.Windows.Forms.TextBox textBoxFactory;
        private System.Windows.Forms.TextBox textBoxManual;
        private System.Windows.Forms.TextBox textBoxWPS;
        private System.Windows.Forms.TextBox textBoxCharge;
        private System.Windows.Forms.TextBox textBoxDisc;
        private System.Windows.Forms.TextBox textBoxStatus;
        private System.Windows.Forms.TextBox textBoxBattValue;
        private System.Windows.Forms.TextBox textBoxTempValue;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBoxTime;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButtonEmergy;
        private System.Windows.Forms.RadioButton radioButtonDischarge;
        private System.Windows.Forms.RadioButton radioButtonTime;
        private System.Windows.Forms.RadioButton radioButtonCharge;
        private System.Windows.Forms.TextBox textBoxCommand;
        private System.Windows.Forms.Label labelConnected;
        private System.Windows.Forms.Label labelDiscon;
        private System.Windows.Forms.TextBox textBox2ndlog;
        private System.Windows.Forms.TextBox textBox1stlog;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBoxMessage;
        private System.Windows.Forms.Label label24;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label labelResult;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label25;
        internal System.Windows.Forms.Button buttonDisconnect;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox textBoxRSSI;
        private System.Windows.Forms.Label label26;
    }
}

