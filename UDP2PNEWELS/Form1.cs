﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using UDP2PNEWELS;
using System.Timers;

namespace UDP2PNEWELS
{
    public partial class Form1 : Form
    {
        //static UDPPROCESSOR myUDP;

        AlertForm alert;
        public Form1()
        {
            InitializeComponent();
        }

        private Thread UDPthread
        {
            get;
            set;
        }

        private Thread analyze
        {
            get;
            set;
        }
        private void buttonConnect_Click(object sender, EventArgs e)
        {
            int port;
            bool gotPort = false;
            string IP = textBoxIP.Text;
            gotPort = int.TryParse(textBoxPort.Text, out port);
            bool alreadyinuse = (from p in System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().GetActiveUdpListeners() where p.Port == port select p).Count() == 1;
            if (alreadyinuse)
                MessageBox.Show("Port in Use!");
            else
            {                
                comboBox1.Text = "1";
                buttonConnect.Visible = false;
                buttonDisconnect.Visible = true;
                labelDiscon.Visible = false;
                labelConnected.Visible = true;
                groupBox1.Visible = true;
                textBoxIP.ReadOnly = true;
                textBoxPort.ReadOnly = true;
                textBoxDeviceAddr.Visible = true;
                label4.Visible = true;
                Program.Globals.log_1 = 0;
                if ((!gotPort) || (String.IsNullOrEmpty(IP)))
                {
                    MessageBox.Show("Insert Correct IP and Port value.");
                }
                else
                {
                    UDPthread = new Thread(() => Program.UdpProcess(IP, port, "+"));
                    UDPthread.Start();
                }
            }
          }

        private void buttonDisconnect_Click(object sender, EventArgs e)
        {
            buttonConnect.Visible = true;
            buttonDisconnect.Visible = false;
            labelDiscon.Visible = true;
            labelConnected.Visible = false;
            groupBox1.Visible = false;
            textBoxIP.ReadOnly = false;
            textBoxPort.ReadOnly = false;
            textBoxDeviceAddr.Visible = false;
            label4.Visible = false;

            if (backgroundWorker1.IsBusy != true)
            {
                // create a new instance of the alert form
                alert = new AlertForm();
                // event handler for the Cancel button in AlertForm
                alert.Canceled += new EventHandler<EventArgs>(buttonCancel_Click);
                alert.Show();
                // Start the asynchronous operation.
                backgroundWorker1.RunWorkerAsync();
            }
            Program.Globals.log_1 = 1;
            UDPthread.Abort();
            UDPthread.Join();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            if (backgroundWorker1.WorkerSupportsCancellation == true)
            {
                // Cancel the asynchronous operation.
                backgroundWorker1.CancelAsync();
                // Close the AlertForm
                alert.Close();
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            for (int i = 1; i <= 10; i++)
            {
                if (worker.CancellationPending == true)
                {
                    e.Cancel = true;
                    break;
                }
                else
                {
                    // Perform a time consuming operation and report progress.
                    worker.ReportProgress(i * 10);
                    System.Threading.Thread.Sleep(500);
                }
            }
        }

        // This event handler updates the progress.
        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // Show the progress in main form (GUI)
            labelResult.Text = (e.ProgressPercentage.ToString() + "%");
            // Pass the progress to AlertForm label and progressbar
            alert.Message = "In progress, please wait... " + e.ProgressPercentage.ToString() + "%";
            alert.ProgressValue = e.ProgressPercentage;
        }

        // This event handler deals with the results of the background operation.
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled == true)
            {
                labelResult.Text = "Canceled!";
            }
            else if (e.Error != null)
            {
                labelResult.Text = "Error: " + e.Error.Message;
            }
            else
            {
                labelResult.Text = "Done!";
            }
            // Close the AlertForm
            alert.Close();
        }

        public void received_data(string output)
        {
            if(InvokeRequired)
            {
                this.Invoke(new Action<string>(received_data), new object[] { output });
            }
            //richTextBox1.Text = output + "\r\n" + ">> " + richTextBox1.Text;
            textBoxDebug.Text = output;//myUDP.Processor(output);
        }

        public void test(char data)
        {
            textBoxDeviceAddr.Text = string.Format("{0:N2}", Convert.ToString(data));
        }

        private void textBoxDebug_TextChanged(object sender, EventArgs e)
        {
            string data_capture = textBoxDebug.Text;
            //MessageBox.Show(data_capture);
            Processor(data_capture);

        }

        public void Processor(string data)
        {
            //byte[] bytes = new byte[data.Length * sizeof(char)];
            char[] chars = new char[data.Length];
            char[] command = new char[6];
            chars = data.ToCharArray();
            uint addr = (uint)(chars[0]);
            uint rssi = 0;         
            //System.Buffer.BlockCopy(data.ToCharArray(), 0, bytes, 0, bytes.Length);
            if (chars[0] == '|')
            {
                uint minus = 0x30;
                //byte rsize = 0;
                addr = ((addr << 8) | ((uint)chars[1] - minus));
                addr = ((addr << 8) | ((uint)chars[2] - minus));
                addr = ((addr << 8) | ((uint)chars[3] - minus));
                addr = ((addr << 8) | ((uint)chars[4] - minus)); //cannot cast to char. encoding error

                rssi = UDPPROCESSOR.hex2uint(chars[5], chars[6]);                
                rssi = (4294967295 - (4294967040 + rssi)) + 1;
                if (Convert.ToUInt32(comboBox1.Text) == addr)
                {
                    if (rssi != 256)
                    {
                        textBoxMessage.Text = null;
                        for (uint i = 0; i < 6; i++)
                        {
                            command[i] = UDPPROCESSOR.hex2unicode(chars[10 + i + i], chars[10 + 1 + i + i]);
                        }
                        string s = new string(command);
                        if (s.Equals("status"))
                        {
                            ProcessData(data);
                        }
                        else if (s.Equals("1stlog"))
                        {
                            string output = UDPPROCESSOR.log_processor(data);
                            textBox1stlog.Text = output;
                        }
                        else if (s.Equals("2ndlog"))
                        {
                            string output = UDPPROCESSOR.log_processor(data);
                            textBox2ndlog.Text = output;
                        }
                        textBoxRSSI.Text = "-" + Convert.ToString(rssi);
                    }
                    else
                    {
                        textBoxMessage.Text = "DEVICE OUT OF RANGE or DEVICE TIMEOUT";
                    }
                }
                
                
            }
        }

        static byte[] GetBytes(string data)
        {
            byte[] bytes = new byte[data.Length * sizeof(char)];
            System.Buffer.BlockCopy(data.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        static byte appHexToInt(byte chr)
        {
            if ('0' <= chr && chr <= '9')
                return (byte)(chr - '0');
            else if ('a' <= chr && chr <= 'f')
                return (byte)(chr - 'a' + 10);
            else if ('A' <= chr && chr <= 'F')
                return (byte)(chr - 'A' + 10);
            return 0;
        }
        
        public void ProcessData(string data)
        {
            char[] chars = new char[data.Length];
            chars = data.ToCharArray();
            uint datapack1 = UDPPROCESSOR.hex2uint(chars[26], chars[27]);
            uint datapack2 = UDPPROCESSOR.hex2uint(chars[32], chars[33]);
            uint datapack3 = UDPPROCESSOR.hex2uint(chars[38], chars[39]);
            uint temperature = UDPPROCESSOR.hex2uint(chars[44], chars[45]);
            uint battvolt = UDPPROCESSOR.hex2uint(chars[50], chars[51]);
            uint time3 = UDPPROCESSOR.hex2uint(chars[62], chars[63]);
            uint time2 = UDPPROCESSOR.hex2uint(chars[68], chars[69]);
            uint time1 = UDPPROCESSOR.hex2uint(chars[74], chars[75]);
            uint time0 = UDPPROCESSOR.hex2uint(chars[80], chars[81]);
            int time = ((int)time3 << 24) + ((int)time2 << 16) + ((int)time1 << 8) + ((int)time0);
            double batt = 1.8 / 255;
            batt = battvolt * batt;
            double temp = 150000.0 / 250000.0;
            batt = batt / temp;

            DateTime epoch = new DateTime(1970, 1, 1);
            DateTime myDateTime = epoch.AddSeconds(time);
            //uint a = Convert.ToUInt16(chars[26]);
            //uint Datapack1_1 = Convert.ToUInt16(chars[27]);

            textBoxAC.Text = ((datapack1 & (1 << 7)) != 0) ? "NOT OK" : "OK";
            textBoxSoftManual.Text = ((datapack1 & (1 << 6)) != 0) ? "PRESSED" : "DEPRESSED";
            textBoxBattStat.Text = ((datapack1 & (1 << 5)) != 0) ? "NOT OK" : "OK";
            textBoxTemp.Text = ((datapack1 & (1 << 4)) != 0) ? "OK" : "NOT OK";
            textBoxVcharge.Text = ((datapack1 & (1 << 3)) != 0) ? "NOT OK" : "OK";
            textBoxFactory.Text = ((datapack1 & (1 << 2)) != 0) ? "DEPRESSED" : "PRESSED";
            textBoxManual.Text = ((datapack1 & (1 << 1)) != 0) ? "PRESSED" : "DEPRESSED";
            textBoxWPS.Text = ((datapack1 & 1) != 0) ? "PRESSED" : "DEPRESSED";
            textBoxDisc.Text = ((datapack3 & (1 << 6)) != 0) ? "ON" : "OFF";
            textBoxCharge.Text = ((datapack3 & (1 << 5)) != 0) ? "ON" : "OFF";
            textBoxStatus.Text = ((datapack2 & 7) == 1) ? "EMERGENCY" : ((datapack2 & 7) == 2) ? "WPS MODE" : ((datapack2 & 7) == 5) ? "STANDBY" : "FACTORY MODE";
            textBoxTempValue.Text = Convert.ToString(temperature);
            textBoxBattValue.Text = batt.ToString();
            textBoxTime.Text = Convert.ToString(myDateTime);
        }

        private void radioButtonEmergy_Click(object sender, EventArgs e)
        {
            if (radioButtonEmergy.Checked)
            {
                
            }
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            string command = null;
            string type = null;
            string addr;
            //type = (radioButtonEmergy.Checked) ? "[emergy]" : (radioButtonTime.Checked) ? "[timech]" : (radioButtonCharge.Checked) ? "[rdchrg]" : "[rddisc]";
            addr = "|" + string.Format("{0:N2}", textBoxDeviceAddr.Text) + ":";
            if (radioButtonEmergy.Checked)
            {
                type = "[emergy]";
                command = addr + Program.ByteToString(0, type);
            }
            if(radioButtonTime.Checked)
            {
                type = "[timech]";
                TimeSpan t = DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Unspecified));
                int epoch = (int)t.TotalSeconds + 28800;
                command = addr + Program.ByteToString(epoch, type);
            }
            if(radioButtonCharge.Checked)
            {
                type = "[rdchrg]";
                command = addr + Program.ByteToString(0, type);
            }
            if(radioButtonDischarge.Checked)
            {
                type = "[rddisc]";
                command = addr + Program.ByteToString(0, type);
            }
            //command = typetype;
            Program.Globals.log_1 = 1;
            textBoxCommand.Text = command;
            UDPthread.Abort();
            UDPthread.Join();
            int port;
            bool gotPort = false;
            string IP = textBoxIP.Text;
            Program.Globals.log_1 = 0;

            buttonConnect.Visible = false;
            buttonDisconnect.Visible = true;

            gotPort = int.TryParse(textBoxPort.Text, out port);

            if ((!gotPort) || (String.IsNullOrEmpty(IP)))
            {
                MessageBox.Show("Insert Correct IP and Port value.");
            }
            else
            {
                UDPthread = new Thread(() => Program.UdpProcess(IP, port, command));
                UDPthread.Start();
                buttonConnect.Visible = false;
                buttonDisconnect.Visible = true;
                labelDiscon.Visible = false;
                labelConnected.Visible = true;
                groupBox1.Visible = true;
                textBoxIP.ReadOnly = true;
                textBoxPort.ReadOnly = true;
                textBoxDeviceAddr.Visible = true;
                label4.Visible = true;
            }
        }

        private void comboBox1_TextChanged(object sender, EventArgs e)
        {
            textBoxAC.Text = null;
            textBoxVcharge.Text = null;
            textBoxBattStat.Text = null;
            textBoxTemp.Text = null;
            textBoxSoftManual.Text = null;
            textBoxFactory.Text = null;
            textBoxManual.Text = null;
            textBoxWPS.Text = null;
            textBoxStatus.Text = null;
            textBoxDisc.Text = null;
            textBoxCharge.Text = null;
            textBoxTempValue.Text = null;
            textBoxBattValue.Text = null;
            textBoxTime.Text = null;

        }
    }

    class UDPPROCESSOR
    {
        public static char hex2unicode(char a, char b)
        {
            //uint character = 0x30;
            uint a1 = Convert.ToUInt16(a);
            uint b1 = Convert.ToUInt16(b);
            if ('0' <= a1 && a1 <= '9')
                a1 = a1 - '0';
            else if ('a' <= a1 && a1 <= 'f')
                a1 =  a1 - 'a' + 10;
            else if ('A' <= a1 && a1 <= 'F')
                a1 = a1 - 'A' + 10;

            if ('0' <= b1 && b1 <= '9')
                b1 = b1 - '0';
            else if ('a' <= b1 && b1 <= 'f')
                b1 = b1 - 'a' + 10;
            else if ('A' <= b1 && b1 <= 'F')
                b1 = b1 - 'A' + 10;
            //a1 = a1 - character;
            //b1 = b1 - character;
            a1 = (a1 << 4);
            uint result = (a1 | b1);
            return Convert.ToChar(result);
        }

        public static uint hex2uint(char a, char b)
        {
            uint a1 = Convert.ToUInt16(a);
            uint b1 = Convert.ToUInt16(b);
            b1 = char2uint(b1);
            a1 = char2uint(a1);
            a1 = (a1 << 4);
            uint result = (a1 | b1);
            return result;
        }

        public static uint char2uint(uint a)
        {
            if ('0' <= a && a <= '9')
                return a - '0';
            else if ('a' <= a && a <= 'f')
                return a - 'a' + 10;
            else if ('A' <= a && a <= 'F')
                return a - 'A' + 10;
            return 0;
        }

        public static string log_processor(string data)
        {
            char[] chars = new char[data.Length];
            chars = data.ToCharArray();
            char log_type_1 = UDPPROCESSOR.hex2unicode(chars[26], chars[27]);
            char log_type_2 = UDPPROCESSOR.hex2unicode(chars[32], chars[33]);
            uint time3 = UDPPROCESSOR.hex2uint(chars[38], chars[39]);
            uint time2 = UDPPROCESSOR.hex2uint(chars[44], chars[45]);
            uint time1 = UDPPROCESSOR.hex2uint(chars[50], chars[51]);
            uint time0 = UDPPROCESSOR.hex2uint(chars[56], chars[57]);
            uint battvolt = UDPPROCESSOR.hex2uint(chars[62], chars[63]);
            uint temperature = UDPPROCESSOR.hex2uint(chars[68], chars[69]);
            uint datapack1 = UDPPROCESSOR.hex2uint(chars[74], chars[75]);
            uint datapack2 = UDPPROCESSOR.hex2uint(chars[80], chars[81]);
            uint datapack3 = UDPPROCESSOR.hex2uint(chars[86], chars[87]);
            int time = ((int)time3 << 24) + ((int)time2 << 16) + ((int)time1 << 8) + ((int)time0);
            double batt = 1.8 / 255;
            batt = battvolt * batt;
            double temp = 150000.0 / 250000.0;
            batt = batt / temp;

            DateTime epoch = new DateTime(1970, 1, 1);
            DateTime myDateTime = epoch.AddSeconds(time);

            string command_type = null;
            if (log_type_1.Equals('C'))
                command_type = "Charging test ";
            else if (log_type_1.Equals('D'))
                command_type = "Discharging test ";
            else if (log_type_1.Equals('E'))
                command_type = "Emergency mode ";
            else if (log_type_1.Equals('B'))
                command_type = "Battery ";

            string status = null;
            if (log_type_2.Equals('S'))
                status = "started ";
            else if (log_type_2.Equals('E'))
                status = "ended ";
            else if (log_type_2.Equals('C'))
                status = "charged ";
            else if (log_type_2.Equals('D'))
                status = "discharged ";

            //textBoxAC.Text = ((datapack1 & (1 << 7)) != 0) ? "NOT OK" : "OK";
            //textBoxSoftManual.Text = ((datapack1 & (1 << 6)) != 0) ? "PRESSED" : "DEPRESSED";
            //textBoxBattStat.Text = ((datapack1 & (1 << 5)) != 0) ? "NOT OK" : "OK";
            //textBoxTemp.Text = ((datapack1 & (1 << 4)) != 0) ? "OK" : "NOT OK";
            //textBoxVcharge.Text = ((datapack1 & (1 << 3)) != 0) ? "NOT OK" : "OK";
            //textBoxFactory.Text = ((datapack1 & (1 << 2)) != 0) ? "DEPRESSED" : "PRESSED";
            //textBoxManual.Text = ((datapack1 & (1 << 1)) != 0) ? "PRESSED" : "DEPRESSED";
            //textBoxWPS.Text = ((datapack1 & 1) != 0) ? "PRESSED" : "DEPRESSED";
            //textBoxDisc.Text = ((datapack3 & (1 << 6)) != 0) ? "ON" : "OFF";
            //textBoxCharge.Text = ((datapack3 & (1 << 5)) != 0) ? "ON" : "OFF";
            //textBoxStatus.Text = ((datapack2 & 7) == 1) ? "EMERGENCY" : ((datapack2 & 7) == 2) ? "WPS MODE" : ((datapack2 & 7) == 5) ? "STANDBY" : "FACTORY MODE";
            //textBoxTempValue.Text = Convert.ToString(temperature);
            //textBoxBattValue.Text = batt.ToString();
            //textBoxTime.Text = Convert.ToString(myDateTime);

            string timestamp = Convert.ToString(myDateTime);
            string battery_level = batt.ToString();
            string temperature_reading = Convert.ToString(temperature);
            string output = command_type + status + "at " + timestamp;
            return output;
        }
    }
}
